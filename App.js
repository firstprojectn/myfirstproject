import React, { Component } from 'react';
import { createSwitchNavigator, createAppContainer } from 'react-navigation'
import Login from './pages/Login';
import Main from './pages/Main';

const AppNavigator = createSwitchNavigator(
  {
    Login,
    Main,
  },
  {
    initialRouteName: 'Login',
  },
);

const AppContainer = createAppContainer(AppNavigator);

export default class App extends Component {
  render() {
    return <AppContainer />
  }
}
