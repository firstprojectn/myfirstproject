import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    KeyboardAvoidingView,
    View,
    Image,
    TextInput,
    TouchableOpacity
} from 'react-native';

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#464f87', //9effe4
        width: '100%',
    },
    container: {
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
    },
    input: {
        height: 50,
        borderWidth: 5,
        borderColor: '#000000',
        width: '60%',
        margin: 10,
        paddingLeft: 10,
        paddingRight: 10,
        backgroundColor: '#d67474', //'#c4ffef',
    },
    button: {
        height: 50,
        width: '60%',
        marginTop: 30,
    },
    image: {
        height: 300,
        width: '90%',
        borderWidth: 5,
        borderColor: '#000000', //'#0062ff'
        marginBottom: 30,
    },
    buttonTouch: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 50,
        width: '60%',
        borderWidth: 5,
        borderColor: '#000000',
        backgroundColor: '#ab3e3e',
        marginTop: 100,
    },
    buttonName: {
        fontWeight: '400',
        fontSize: 20,
    },
});

export default class Main extends Component {

    constructor(props) {
        super(props);
        this.state = {
            interval: 3000,
            image: 'https://images.dog.ceo/breeds/setter-gordon/n02101006_1208.jpg'
        };
        this.IsMounted = false;
    }

    componentDidMount() {
        this.IsMounted = true;
        this.slideshow = setInterval(this.getImage, this.state.interval);
    }

    getImage = () => {
        fetch('https://dog.ceo/api/breed/mix/images/random')
            .then((res) => res.json())
            .then((data) => {
                if (data.status === 'success' && this.IsMounted) {
                    this.setState({ image: data.message });
                } else throw new Error('Request is not successfull');
            })
            .catch(alert);
    }

    componentDidUpdate() {
        clearInterval(this.slideshow);
        this.slideshow = setInterval(this.getImage, this.state.interval);
    }

    componentWillUnmount() {
        this.IsMounted = false;
        clearInterval(this.slideshow);
    }

    onChangeInterval = (seconds) => {
        if (seconds === '') {
            alert('Please enter a number');
            return;
        } else if (isNaN(seconds) || isNaN(parseInt(seconds))) {
            alert('It is not a numger. Please enter again');
            return;
        } else if (parseInt(seconds) < 1 || parseInt(seconds) > 30) {
            alert('Please enter a number from 1 to 30');
            return;
        } else {
            if (this.IsMounted) {
                let newInterval = parseInt(seconds) * 1000;
                this.setState({ interval: newInterval });
            } else return;
        }
    }

    render() {

        return (
            <View style={styles.mainContainer}>
                <Image
                    resizeMode='contain'
                    style={styles.image}
                    source={{ uri: this.state.image }} />
                <KeyboardAvoidingView behavior='padding' style={styles.container}>
                    <TextInput style={styles.input}
                        keyboardType='numeric'
                        maxLength={2}
                        onChangeText={interval => this.onChangeInterval(interval)}
                        placeholder='Enter another interval in seconds' />
                </KeyboardAvoidingView>
                <TouchableOpacity style={styles.buttonTouch}
                    onPress={() => this.props.navigation.navigate('Login')} >
                    <Text style={styles.buttonName}>EXIT</Text>
                </TouchableOpacity>

            </View>
        );
    }
};
