import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    KeyboardAvoidingView,
    TextInput,
    TouchableOpacity
} from 'react-native';

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#464f87', //9effe4
        width: '100%',
    },
    container: {
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
    },
    text: {
        textAlign: 'center',
        textAlignVertical: 'center',
        borderWidth: 5,
        borderColor: '#000000',  //'#0062ff'
        padding: 20,
        margin: 40,
        marginBottom: 70,
        backgroundColor: '#ab3e3e',
        fontSize: 28,
        fontWeight: 'bold',
    },
    input: {
        height: 50,
        borderBottomColor: '#000000',  //'#0062ff'
        borderBottomWidth: 2,
        width: '60%',
        margin: 10,
        //backgroundColor: '#c4ffef', //'#c4ffef',#8c2727
    },
    buttonTouchSI: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 40,
        width: '60%',
        backgroundColor: '#f26b9a',//'#ff5cd6',
        marginTop: 70,
        borderRadius: 5,
    },
    buttonNameSI: {
        fontSize: 20,
        color: '#ffffff'
    },
    textFP: {
        marginTop: 50,
        color: '#c7c0bf',//'#ccc5c4',//'#d1cccb',
    },
    buttonTouchSU: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 40,
        width: '60%',
        marginTop: 20,
        borderRadius: 5,
        borderWidth: 2,
        borderColor: '#c7c0bf',
    },
    buttonNameSU: {
        fontSize: 20,
        color: '#c7c0bf'
    },
    textSR: {
        marginTop: 40,
        color: '#ffffff',//'#ccc5c4',
    },
});

export default class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
        };
    }

    onPressButton = () => {
        if (this.state.email !== '' && this.state.password !== '') {
            let regexp = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
            if (regexp.test(this.state.email) && this.state.password.length < 6) {
                alert('Check your user name and passord.'
                    + ' User name is email. The password must contain at least 6 characters');
            } else {
                this.props.navigation.navigate('Main');
            }
        } else {
            alert('Please enter your user name and password');
        }
    }

    render() {
        return (
            <View style={styles.mainContainer}>
                <View style={styles.mainContainer}>
                    <Text style={styles.text}>MyFirstProject</Text>
                    <KeyboardAvoidingView behavior='padding' style={styles.container}>
                        <TextInput style={styles.input}
                            keyboardType='email-address'
                            onChangeText={(email) => this.setState({ email })}
                            maxLength={50}
                            placeholder='User Name' />
                        <TextInput style={styles.input}
                            onChangeText={(password) => this.setState({ password })}
                            maxLength={20}
                            secureTextEntry
                            placeholder='Password' />
                    </KeyboardAvoidingView>
                    <TouchableOpacity>
                        <Text style={styles.textFP}>Forgot password?</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.buttonTouchSI}
                        onPress={this.onPressButton} >
                        <Text style={styles.buttonNameSI}>SIGN IN</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.buttonTouchSU} >
                        <Text style={styles.buttonNameSU}>SIGN UP</Text>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Text style={styles.textSR}>SKIP REGISTRATION ></Text>
                    </TouchableOpacity>
                </View>
            </View >
        );
    };
}