### This is react native app with dog gallery
###### Instruction:
1. install react native (https://facebook.github.io/react-native/docs/getting-started);
2. clone a project from https://bitbucket.org/firstprojectn/myfirstproject/src/master/;
3. run the application on the command prompt using the command "expo start" or "npm start";
4. you can test this app using the application "expo" on your mobile phone (scan QR-code  of tunnel connection) or some emulator for PC;
5. you get to the registration page after starting the application on the emulator (you must enter the correct username and password for sign in);
6. after that the main page opens, where by default a random image is displayed every 3 seconds. You can change this interval by typing new interval (it's a namber from 1 to 30 seconds);
7. if you want to return to the registration page click "Exit".